<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SIST_MST_PREGUNTA', function (Blueprint $table) {
            $table->bigIncrements('PK_PREG_NIDPREGUNTA');
            $table->string('PREG_CDESCRIPCION');
            $table->integer('PREG_NTIPO');
            $table->string('PREG_CDESCRIPCION');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SIST_MST_PREGUNTA');
    }
}

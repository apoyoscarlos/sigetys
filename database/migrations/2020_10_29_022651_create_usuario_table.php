<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('tipo_persona', ['FISICA', 'MORAL'])->default('FISICA');
            $table->string('rfc', 200)->nullable();
            $table->string('razon_social', 50)->nullable();
            $table->enum('sexo', ['M', 'F'])->nullable();
            $table->string('nombres', 50);
            $table->string('apellido_paterno', 150);
            $table->string('apellido_materno', 150)->nullable();
            $table->string('calle', 250)->nullable();
            $table->string('numero_interior', 10)->nullable();
            $table->string('numero_exterior', 10)->nullable();

            //Los estoy poniendo como string pero seran llaves foraneas
            $table->string('codigo_postal', 10)->nullable();
            $table->string('colonia', 10)->nullable();
            $table->string('municipio', 10)->nullable();
            $table->string('estado', 10)->nullable();
            $table->string('pais', 10)->nullable();

            $table->string('correo_electronico', 150);
            $table->string('correo_alternativo', 150)->nullable();
            $table->string('password');


            // No se si pondran roles y permisos
            /* $table->bigInteger('rol_id')->nullable()->unsigned()->index(); */
            
            $table->timestamps();


            //Referencias a las llaves foraneas
            /* $table->foreign('rol_id')->references('id')->on('roles')
                ->onDelete('cascade')
                ->onUpdate('cascade'); */

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}

<nav id="sidebar">
    <div class="sidebar-header">
        <a class="navbar-brand linea Texto-menu" href="inicio.html">
            <img src="{{ asset('assets/template/img/logo_edo.png') }}" width="40" height="40" class="d-inline-block align-top" alt="">
        </a>
        <a style="color: #000;" href="inicio.html" id="nomApp" class="navbar-text">Sistema de citas</a>
    </div>
    <ul id="dropMenu" class="list-unstyled components">
        <li class="active"><a href='inicio.html'>Inicio</a></li>
        <li><a href="#">Seguimiento a solicitudes</a></li>
        <li><a href="#">Gestores</a></li>
        <li><a href="#">Lista de trámites</a></li>
        <li><a href="#">Servidores públicos</a></li>
        <li><a href="#">Personas físicas o morales</a></li>
        <li><a href="#">Formularios</a></li>
        <li><a href="#">Reportes estadísticos</a></li>
        <li><a href='#homeSubmenu00' data-toggle='collapse' aria-expanded='false'
                class='dropdown-toggle'>Configuraciones</a>
            <ul class='collapse list-unstyled' id='homeSubmenu00'>
                <li><a href='usuarios.html'>Usuarios</a></li>
                <li><a href='#'>Días inhábiles</a></li>
                <li><a href='#'>Vigencias</a></li>
                <li><a href='#'>Notificaciones</a></li>
            </ul>
        </li>
    </ul>
</nav>


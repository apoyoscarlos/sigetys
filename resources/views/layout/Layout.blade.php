<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistemas de estandares</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="{{ asset('assets/template/img/icon-edo.png') }}" rel="shortcut icon">
    <link href="{{ asset('assets/template/css/bootstrap.css') }}" rel="stylesheet">

    {{-- No los tiene el template
        <link rel="stylesheet" href="plugins/date/css/bootstrap-datetimepicker.min.css" />
        <link rel="stylesheet" href="plugins/date/css/bootstrap-datepicker3.min.css" />
        <link rel="stylesheet" href="plugins/date/css/bootstrap-datetimepicker-standalone.css" />

        <link rel="stylesheet" href="plugins/mdb/css/mdb.min.css">
    --}}

    <link href="{{ asset('assets/template/plugins/DataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/template/css/formvalidation.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/template/css/bootstrap-combobox.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/template/css/fonts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/template/css/style.css') }}" rel="stylesheet">

</head>

<body>
    <div class="wrapper">        
       @include('layout.Navbar')

        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light lighten-4 ">
                <div class="float-left" id="sidebarCollapse">
                    <a class="button-collapse" href="#!" data-activates="slide-out"><i
                            class="fas fa-bars fa-lg"></i></a>
                </div>
        
                <!-- Breadcrumb-->
                <div class="mr-auto">
                    <nav aria-label="breadcrumb ">
                        <ol class="breadcrumb ">
                            <li class="breadcrumb-item"><a href="#!">Inicio</a></li>
                            <!-- breadcrumb de mas -->
                        </ol>
                    </nav>
                </div>
        
        
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
        
        
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                AlanGtzl
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <!-- <%--<a class="dropdown-item" href="#">Ayuda</a>--%> -->
                                <a class="dropdown-item" download="ManualCitas.pdf" 
                                    href="docs/ManualCitas.pdf">Manual</a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#mdlPrivacidad" href="#">Aviso
                                    de privacidad</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="index.html">Cerrar sesión</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

           @yield('body')

           <!-- Modal de aviso de provacidad -->
            <div class="modal fade" id="mdlPrivacidad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body" style="font-size: 12px">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h4 class="bold">AVISO DE PRIVACIDAD INTEGRAL</h4>
                                    <h4 class="bold">SISTEMA DE GESTIÓN CIUDADANA DE TRÁMITES Y SERVICIOS</h4>
                                </div>
                            </div>
                            <hr style="background:#f7af00; height: 3px;" />
                            <h6 class="bold">ACERCA DE LA SECRETARÍA DE LA FUNCIÓN PÚBLICA </h6>
                            La Secretaría de la Función Pública es la Dependencia del Poder Ejecutivo del Estado de
                            Chihuahua, responsable de diseñar, dirigir y coordinar la implementación de la política
                            pública de mejora regulatoria con las dependencias y entidades de la Administración Pública
                            Estatal, enfocándose en la reducción de cargas administrativas en los trámites y servicios
                            del Estado.
                            <br />
                            <br />
                            Asimismo, le compete coordinar la estrategia de digitalización de los trámites y servicios
                            de la Administración Pública Estatal, así como consolidar el uso y aprovechamiento de las
                            tecnologías de la información para fomentar canales directos de vinculación con la
                            ciudadanía.
                            <br />
                            <br />
                            Esta Dependencia es la responsable del uso y protección de sus datos personales, y se
                            localiza en la Calle Victoria número 310, Colonia Centro, Código Postal 31000, Edificio Lic.
                            Oscar Flores, en la Ciudad de Chihuahua, Chihuahua.
                            <br />
                            <br />
                            <h6 class="bold">LOS DATOS PERSONALES QUE SERÁN RECABADOS DE USTED EN EL SISTEMA DE GESTIÓN
                                CIUDADANA DE TRÁMITES Y SERVICIOS, SERÁN UTILIZADOS PARA LAS SIGUIENTES FINALIDADES:
                            </h6>

                            1. Contar con un registro de las personas servidoras públicas que brindan atención a la
                            ciudadanía por medio de citas en línea.<br />
                            2. Contactarle en caso de algún problema relacionado con las citas en línea.<br />
                            <br />

                            La información proporcionada a través de este sistema puede ser incluida en los informes que
                            se elaboran para el seguimiento de avances institucionales, los cuales serán meramente
                            estadísticos y no incluirán información que permita identificarle en lo individual.
                            <br />
                            <br />
                            <h6 class="bold">¿QUÉ DATOS PERSONALES UTILIZAREMOS EN EL SISTEMA DE GESTIÓN CIUDADANA DE
                                TRÁMITES Y SERVICIOS? </h6>

                            Para llevar a cabo las finalidades descritas en el presente aviso de privacidad,
                            utilizaremos los siguientes datos personales:
                            <br />

                            • Nombre<br />
                            • Correo electrónico<br />
                            • Número de contacto<br />
                            <br />
                            Es preciso señalar que se le podrá solicitar algún dato adicional, a fin de llevar a cabo
                            las finalidades establecidas.
                            <br />
                            <br />
                            <h6 class="bold">¿CON QUIÉN COMPARTIMOS SU INFORMACIÓN PERSONAL Y PARA QUÉ FINES?</h6>

                            Los datos personales que se ingresen en el Sistema de Gestión Ciudadana de Trámites y
                            Servicios, podrán ser transferidos únicamente a personal de su Dependencia o Entidad de
                            adscripción.
                            <br />
                            <br />
                            No serán difundidos, distribuidos o comercializados con terceros. Únicamente podrán ser
                            proporcionados a terceros previa autorización del titular de los mismos y bajo alguno de los
                            supuestos que contempla el artículo 20 de la Ley de Protección de Datos Personales del
                            Estado de Chihuahua.
                            <br />
                            <br />
                            <h6 class="bold">FUNDAMENTO JURÍDICO</h6>

                            En cumplimiento a lo establecido por los artículos 6, apartado A, de la Constitución
                            Política de los Estados Unidos Mexicanos, 4, fracción III, de la Constitución Política del
                            Estado de Chihuahua, 3 fracción II, 16, 18, 20 fracción III, 21, 26, 27, 28 y 69 de la Ley
                            General de Protección de Datos Personales en Posesión de Sujetos Obligados, 11, fracciones
                            I, II, XVI, 16, fracción II, 17, fracción III, 63, 65, 67, 91, fracción II, apartado a), de
                            la Ley de Protección de Datos Personales del Estado de Chihuahua y demás legislación
                            relativa y aplicable en la materia.
                            <br />
                            <br />
                            <h6 class="bold">¿CÓMO PUEDE ACCEDER, RECTIFICAR O CANCELAR SUS DATOS PERSONALES, U OPONERSE
                                A SU USO?</h6>

                            La persona usuaria del Sistema de Gestión Ciudadana de Trámites y Servicios, tiene derecho a
                            conocer cuáles de sus datos personales se encuentran registrados, para qué los utilizamos y
                            las condiciones del uso que les damos (Acceso). Asimismo, es su derecho solicitar la
                            corrección de su información personal en caso de que esté desactualizada, sea inexacta o
                            incompleta (Rectificación); que la eliminemos de nuestros registros o bases de datos cuando
                            considere que la misma no está siendo utilizada conforme a los principios, deberes y
                            obligaciones previstas en la normativa (Cancelación); así como oponerse al uso de sus datos
                            personales para fines específicos (Oposición). Estos derechos se conocen como derechos ARCO.
                            <br />
                            <br />
                            La persona usuaria podrá ejercer sus Derechos de Acceso, Rectificación, Cancelación,
                            Oposición (ARCO), así como negativa al tratamiento de los mismos, ante la Unidad de
                            Transparencia de la Secretaría de la Función Pública, ubicada en Calle Victoria número 310
                            segundo piso, Colonia Centro, Código Postal 31000, Edificio Lic. Oscar Flores, en la Ciudad
                            de Chihuahua, Chih., con teléfono 614 429 3300 extensión 20382, de lunes a viernes, de 8:00
                            horas a las 16:00 horas, o por el correo electrónico
                            unidadtransparencia.sfp@chihuahua.gob.mx y a través de la Plataforma Nacional de
                            Transparencia
                            <a href="http://www.plataformadetransparencia.org.mx/"
                                target="_blank">http://www.plataformadetransparencia.org.mx/</a>

                            <br />
                            <br />
                            <h6 class="bold">¿CÓMO PUEDE CONOCER LOS CAMBIOS A ESTE AVISO DE PRIVACIDAD?</h6>

                            El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones
                            derivadas de nuevos requerimientos legales, por mejoras en el Sistema de Gestión Ciudadana
                            de Trámites y Servicios o por otras causas.
                            <br />
                            <br />
                            Nos comprometemos a mantenerlo informado sobre los cambios que pueda sufrir el presente
                            aviso de privacidad a través del Sistema de Gestión Ciudadana de Trámites y Servicios.

                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <script src="{{ asset('assets/template/js/jquery.js') }}"></script>
    {{--  <script type="text/javascript" src="js/popper.min.js"></script> --}}
    <script src="{{ asset('assets/template/js/bootstrap.min.js') }}"></script>
    {{-- <script type="text/javascript" src="plugins/mdb/js/mdb.min.js"></script> --}}
    <script src="{{ asset('assets/template/plugins/DataTables/datatables.min.js') }}"></script>
    {{-- <script type="text/javascript" src="plugins/fontawesome-5.9.0/js/all.min.js"></script> --}}
    <script src="{{ asset('assets/template/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/formValidation.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/framework/bootstrap.min.js') }}"></script>
    {{-- <script type="text/javascript" src="plugins/sweetalert2/sweetalert2.js"></script> --}}
    <script src="{{ asset('assets/template/js/bootstrap-combobox.js') }}"></script>
    <script src="{{ asset('assets/template/js/Moment.js') }}"></script>
    {{-- <script src="plugins/date/js/bootstrap-datepicker.min.js"></script> --}}
    {{-- <script src="plugins/date/js/bootstrap-datetimepicker.min.js"></script> --}}

    @yield('scripts')
</body>

</html>
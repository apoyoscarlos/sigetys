<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuario';
    protected $fillable = [
        'tipo_persona',
        'rfc',
        'razon_social',
        'sexo',
        'nombres',
        'apellido_paterno',
        'apellido_materno',
        'calle',
        'numero_interior',
        'numero_exterior',
        'codigo_postal',
        'colonia',
        'municipio',
        'estado',
        'pais',
        'correo_electronico',
        'correo_alternativo',
        'password'
    ];

    protected $hidden = ['password'];
}
